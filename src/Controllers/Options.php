<?
namespace DSite\AdminInterface\Controllers;
use Bitrix\Main\SystemException;
use DSite\AdminInterface\Models\Widgets\Demo;
use DSite\AdminInterface\Models\Widgets\Form;

class Options extends Form{
    /**
     * @var string
     */
    protected $site = '';
    /**
     * @var string
     */
    protected $module = '';

    /**
     * Options constructor.
     * @param array $vars
     */
    public function __construct($vars = [])
    {
        try{
            if(!$vars['module']){
                throw new SystemException("Unknown module");
            }
            $this->setModule($vars['module']);
            if($vars['site'])
                $this->setSite($vars['site']);
        }
        catch (SystemException $exception)
        {
            echo $exception->getMessage();
        }
    }

    /**
     * @param array $fields
     */
    public function show(array $fields = []){
        $Demo = new Demo();
        $Demo->show($this->getModule());

        if($fields){
            $tabControl = new \CAdminTabControl('tabControl', $this->prepareTabs($fields));
            $tabControl->Begin();
            $Form = new Form();
            $Form->open($this->getModule());
            foreach($fields as $tabs){
                $tabControl->BeginNextTab();
                if($tabs['groups']){
                    foreach($tabs['groups'] as $group){
                        $Form->showSection($group['title']);
                        if($group['fields']){
                            foreach($group['fields'] as $code => $field){
                                $Class = '\\DSite\\AdminInterface\\Models\\Widgets\\'.ucfirst($field['type']);
                                $Class = new $Class();
                                $Class->show(
                                    [
                                        'title' => $field['title'],
                                        'code' => $code
                                    ]
                                );
                            }
                        }
                    }
                }
            }
            $tabControl->Buttons();
            $Form->close();
            $tabControl->End();
        }
    }

    private function prepareTabs(array $fields = []): array{
        $tabs = [];
        foreach ($fields as $i=>$tab)
        {
            $tabs[] = array(
                'DIV' => 'edit_access_tab_'.$i,
                'TAB' => $tab['title'],
                'ICON' => '',
                'TITLE' => $tab['title'],
            );
        }
        return $tabs;
    }

    /**
     * @return string
     */
    public function getSite(): string
    {
        return $this->site;
    }

    /**
     * @param string $site
     */
    public function setSite(string $site): void
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule(string $module): void
    {
        $this->module = $module;
    }
}