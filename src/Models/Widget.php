<?php

namespace DSite\AdminInterface\Models;

use Bitrix\Main\Localization\Loc;

class Widget
{
    protected $viewsDir = __DIR__.'/../Views/';
    /**
     * @param string $name
     * @param array $params
     * @return bool|false|mixed|string
     */
    public function showView($name = '',$params = []){
        if(!file_exists($this->getViewsDir().$name.'.html')){
            return '';
        }
        Loc::loadLanguageFile($this->getViewsDir().$name.'.php');
        $content = file_get_contents($this->getViewsDir().$name.'.html');

        return $this->changeContent($content,$params);
    }

    /**
     * @param string $content
     * @param array $params
     * @return string
     */
    protected function changeContent(string $content = '', array $params = []):string {
        $content = $this->changeLangs($content);
        $content = $this->changeVars($content, $params);
        return $content;
    }

    /**
     * @param string $content
     * @return string
     */
    protected function changeLangs(string $content = ''):string {
        preg_match_all('#\{{.*?}}#', $content, $matches);
        if($matches[0]){
            foreach ($matches[0] as $m){
                $messCode = str_replace(['{{','}}'],'',$m);
                if(Loc::getMessage($messCode))
                    $content = str_replace($m,Loc::getMessage($messCode),$content);
            }
        }
        return $content;
    }

    /**
     * @param string $content
     * @param array $params
     * @return string
     */
    protected function changeVars(string $content = '', array $params = []):string {
        if($params){
            foreach ($params as $key => $value){
                $content = str_replace('{{'.$key.'}}',$value,$content);
            }
        }
        return $content;
    }

    /**
     * @return string
     */
    public function getViewsDir(): string
    {
        return $this->viewsDir;
    }

}