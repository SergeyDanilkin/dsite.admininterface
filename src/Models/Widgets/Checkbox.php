<?php

namespace DSite\AdminInterface\Models\Widgets;
use DSite\AdminInterface\Models\Widget;

/**
 * Class Checkbox
 * @package DSite\AdminInterface\Widgets
 */
class Checkbox extends Widget {
    /**
     * @param array $fields
     */
    public function show(array $fields = []){
        echo parent::showView('checkbox',$fields);
    }
}
?>