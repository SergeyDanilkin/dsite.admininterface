<?php

namespace DSite\AdminInterface\Models\Widgets;
use DSite\AdminInterface\Models\Widget;

/**
 * Class Form
 * @package DSite\AdminInterface\Widgets
 */
class Form extends Widget {
    /**
     * @param string $module
     */
    public function open(string $module = ''){
        echo parent::showView('form_open');
        bitrix_sessid_post();
    }
    public function close(string $module = ''){
        echo parent::showView('form_close');
    }
    public function showSection(string $title = ''){
        echo parent::showView('form_section',['title' => $title]);
    }
}
?>