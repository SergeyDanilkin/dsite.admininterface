<?php

namespace DSite\AdminInterface\Models\Widgets;
use Bitrix\Main\Loader;
use DSite\AdminInterface\Models\Widget;

/**
 * Class Demo
 * @package DSite\AdminInterface\Widgets
 */
class Demo extends Widget {
    /**
     * @param string $module
     */
    public function show(string $module = ''){
        $demo = Loader::includeSharewareModule( $module );
        if($demo == Loader::MODULE_DEMO){
            echo parent::showView('demo',['module' => $module]);
        }
        elseif ($demo == Loader::MODULE_DEMO_EXPIRED){
            echo parent::showView('demo_end',['module' => $module]);
            return;
        }
    }
}
?>